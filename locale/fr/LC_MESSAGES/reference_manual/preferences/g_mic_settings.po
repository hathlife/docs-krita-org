msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-02 03:06+0200\n"
"PO-Revision-Date: 2019-02-27 03:10+0100\n"
"Last-Translator: KDE Francophone <kde-francophone@kde.org>\n"
"Language-Team: KDE Francophone <kde-francophone@kde.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Lokalize 1.5\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"
"X-Environment: kde\n"
"X-Language: fr_FR\n"
"X-Qt-Contexts: true\n"
"Generated-By: Babel 0.9.6\n"
"X-Source-Language: C\n"

#: ../../reference_manual/preferences/g_mic_settings.rst:1
msgid "How to setup G'Mic in Krita."
msgstr ""

#: ../../reference_manual/preferences/g_mic_settings.rst:10
msgid "Preferences"
msgstr ""

#: ../../reference_manual/preferences/g_mic_settings.rst:10
msgid "Settings"
msgstr ""

#: ../../reference_manual/preferences/g_mic_settings.rst:10
msgid "Filters"
msgstr ""

#: ../../reference_manual/preferences/g_mic_settings.rst:10
msgid "G'Mic"
msgstr ""

#: ../../reference_manual/preferences/g_mic_settings.rst:15
msgid "G'Mic Settings"
msgstr ""

#: ../../reference_manual/preferences/g_mic_settings.rst:17
msgid ""
"G'Mic or GREYC's Magic for Image Computing is an opensource filter "
"framework, or, it is an extra program you can download to have access to a "
"whole lot of image filters."
msgstr ""

#: ../../reference_manual/preferences/g_mic_settings.rst:19
msgid ""
"Krita has had G'Mic integration for a long time, but this is its most stable "
"incarnation."
msgstr ""

#: ../../reference_manual/preferences/g_mic_settings.rst:21
msgid "You set it up as following:"
msgstr ""

#: ../../reference_manual/preferences/g_mic_settings.rst:23
msgid ""
"First download the proper krita plugin from `the G'Mic website. <https://"
"gmic.eu/download.shtml>`_."
msgstr ""

#: ../../reference_manual/preferences/g_mic_settings.rst:24
msgid "Then, unzip and place it somewhere you can find it."
msgstr ""

#: ../../reference_manual/preferences/g_mic_settings.rst:25
msgid ""
"Go to :menuselection:`Settings --> Configure Krita --> G'Mic plugin` and set "
"G'MIC to the filepath there."
msgstr ""

#: ../../reference_manual/preferences/g_mic_settings.rst:26
msgid "Then restart Krita."
msgstr ""

#: ../../reference_manual/preferences/g_mic_settings.rst:30
msgid "Updates to G'Mic"
msgstr ""

#: ../../reference_manual/preferences/g_mic_settings.rst:32
msgid ""
"There is a refresh button at the bottom of the G'Mic window that will update "
"your version. You will need an internet connection to download the latest "
"version."
msgstr ""

#: ../../reference_manual/preferences/g_mic_settings.rst:34
msgid ""
"If you have issues downloading the update through the plugin, you can also "
"do it manually. If you are trying to update and get an error, copy the URL "
"that is displayed in the error dialog. It will be to a \".gmic\" file. "
"Download it from from your web browser and place the file in one of the "
"following directories."
msgstr ""

#: ../../reference_manual/preferences/g_mic_settings.rst:36
msgid "Windows : %APPDATA%/gmic/update2XX.gmic"
msgstr ""

#: ../../reference_manual/preferences/g_mic_settings.rst:37
msgid "Linux : $HOME/.config/gmic/update2XX.gmic"
msgstr ""

#: ../../reference_manual/preferences/g_mic_settings.rst:39
msgid ""
"Load up the G'Mic plugin and press the refresh button for the version to "
"update."
msgstr ""
