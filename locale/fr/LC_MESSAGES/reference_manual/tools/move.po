msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-02 03:06+0200\n"
"PO-Revision-Date: 2019-02-27 08:08+0100\n"
"Last-Translator: KDE Francophone <kde-francophone@kde.org>\n"
"Language-Team: KDE Francophone <kde-francophone@kde.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Lokalize 1.5\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"
"X-Environment: kde\n"
"X-Language: fr_FR\n"
"X-Qt-Contexts: true\n"
"Generated-By: Babel 0.9.6\n"
"X-Source-Language: C\n"

#: ../../<generated>:1
msgid "Position"
msgstr "Position"

#: ../../<rst_epilog>:44
msgid ""
".. image:: images/icons/move_tool.svg\n"
"   :alt: toolmove"
msgstr ""

#: ../../reference_manual/tools/move.rst:0
msgid ".. image:: images/tools/Movetool_coordinates.png"
msgstr ""

#: ../../reference_manual/tools/move.rst:1
msgid "Krita's move tool reference."
msgstr ""

#: ../../reference_manual/tools/move.rst:11
msgid "Tools"
msgstr ""

#: ../../reference_manual/tools/move.rst:11
#, fuzzy
#| msgid "Move Tool"
msgid "Move"
msgstr "Déplacer"

#: ../../reference_manual/tools/move.rst:11
msgid "Transform"
msgstr ""

#: ../../reference_manual/tools/move.rst:16
msgid "Move Tool"
msgstr "Déplacer"

#: ../../reference_manual/tools/move.rst:18
msgid "|toolmove|"
msgstr ""

#: ../../reference_manual/tools/move.rst:20
msgid ""
"With this tool, you can move the current layer or selection by dragging the "
"mouse."
msgstr ""

#: ../../reference_manual/tools/move.rst:22
msgid "Move current layer"
msgstr "Déplacer le calque sélectionné"

#: ../../reference_manual/tools/move.rst:23
msgid "Anything that is on the selected layer will be moved."
msgstr ""

#: ../../reference_manual/tools/move.rst:24
msgid "Move layer with content"
msgstr "Déplacer un calque avec contenu"

#: ../../reference_manual/tools/move.rst:25
msgid ""
"Any content contained on the layer that is resting under the four-headed "
"Move cursor will be moved."
msgstr ""

#: ../../reference_manual/tools/move.rst:26
msgid "Move the whole group"
msgstr "Déplacer tout le groupe"

#: ../../reference_manual/tools/move.rst:27
msgid ""
"All content on all layers will move.  Depending on the number of layers this "
"might result in slow and, sometimes, jerky movements. Use this option "
"sparingly or only when necessary."
msgstr ""

#: ../../reference_manual/tools/move.rst:28
msgid "Shortcut move distance (3.0+)"
msgstr ""

#: ../../reference_manual/tools/move.rst:29
msgid ""
"This allows you to set how much, and in which units, the :kbd:`Left Arrow`, :"
"kbd:`Up Arrow`, :kbd:`Right Arrow` and :kbd:`Down Arrow` cursor key actions "
"will move the layer."
msgstr ""

#: ../../reference_manual/tools/move.rst:30
msgid "Large Move Scale (3.0+)"
msgstr ""

#: ../../reference_manual/tools/move.rst:31
msgid ""
"Allows you to multiply the movement of the Shortcut Move Distance when "
"pressing the :kbd:`Shift` key before pressing a direction key."
msgstr ""

#: ../../reference_manual/tools/move.rst:32
msgid "Show coordinates"
msgstr ""

#: ../../reference_manual/tools/move.rst:33
msgid ""
"When toggled will show the coordinates of the top-left pixel of the moved "
"layer in a floating window."
msgstr ""

#: ../../reference_manual/tools/move.rst:35
msgid ""
"If you click, then press the :kbd:`Shift` key, then move the layer, movement "
"is constrained to the horizontal and vertical directions. If you press the :"
"kbd:`Shift` key, then click, then move, all layers will be moved, with the "
"movement constrained to the horizontal and vertical directions"
msgstr ""

#: ../../reference_manual/tools/move.rst:37
msgid "Constrained movement"
msgstr ""

#: ../../reference_manual/tools/move.rst:40
msgid ""
"Gives the top-left coordinate of the layer, can also be manually edited."
msgstr ""
