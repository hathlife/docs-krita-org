msgid ""
msgstr ""
"Project-Id-Version: kdeorg\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-08-16 17:04\n"
"Last-Translator: Guo Yunhe (guoyunhe)\n"
"Language-Team: Chinese Simplified\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: crowdin.com\n"
"X-Crowdin-Project: kdeorg\n"
"X-Crowdin-Language: zh-CN\n"
"X-Crowdin-File: /kf5-trunk/messages/www/"
"docs_krita_org_reference_manual___brushes___brush_engines.pot\n"

#: ../../reference_manual/brushes/brush_engines.rst:5
msgid "Brush Engines"
msgstr "笔刷引擎"

#: ../../reference_manual/brushes/brush_engines.rst:7
msgid ""
"Information on the brush engines that can be accessed in the brush editor."
msgstr ""
"介绍在笔刷编辑器中可以使用的笔刷引擎。由于文档系统的限制，笔刷引擎按照英文名"
"进行排序。注意：本分类介绍的是 Krita 的笔刷引擎以及相关选项，并非软件自带的笔"
"刷预设。如需了解各个自带笔刷的用法，可参考 :ref:`krita_4_preset_bundle` 。"

#: ../../reference_manual/brushes/brush_engines.rst:9
msgid "Available Engines:"
msgstr "可用的笔刷引擎："
