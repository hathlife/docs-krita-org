# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
#
# Stefan Asserhäll <stefan.asserhall@bredband.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.2\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-05 03:40+0200\n"
"PO-Revision-Date: 2019-05-30 12:13+0100\n"
"Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>\n"
"Language-Team: Swedish <kde-i18n-doc@kde.org>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 2.0\n"

#: ../../<generated>:1
msgid "Angle"
msgstr "Vinkel"

#: ../../reference_manual/clones_array.rst:1
msgid "The Clones Array functionality in Krita"
msgstr "Funktionen Duplicerar fält i Krita"

#: ../../reference_manual/clones_array.rst:10
#: ../../reference_manual/clones_array.rst:15
msgid "Clones Array"
msgstr "Duplicerar fält"

#: ../../reference_manual/clones_array.rst:10
msgid "Clone"
msgstr "Duplicera"

#: ../../reference_manual/clones_array.rst:17
msgid ""
"Allows you to create a set of clone layers quickly. These are ordered in "
"terms of rows and columns. The default options will create a 2 by 2 grid. "
"For setting up tiles of an isometric game, for example, you'd want to set "
"the X offset of the rows to half the value input into the X offset for the "
"columns, so that rows are offset by half. For a hexagonal grid, you'd want "
"to do the same, but also reduce the Y offset of the grids by the amount of "
"space the hexagon can overlap with itself when tiled."
msgstr ""
"Gör det möjligt att snabbt skapa en mängd dupliceringslager. De är ordnade i "
"form av rader och kolumner. Standardalternativen skapar ett 2 gånger 2 "
"rutnät. För att exempelvis skapa rutorna i ett isometriskt spel, skulle man "
"vilja ställa in radernas X-position till halva värdet inmatat för "
"kolumnernas X-position, så att raderna är förflyttade ett halvt. För ett "
"sexkantigt rutnät skulle man vilja göra samma sak, men också reducera "
"rutnätets Y-position med det mellanrum som sexhörningen kan överlappa med "
"sig själv när den läggs ut."

#: ../../reference_manual/clones_array.rst:19
msgid "\\- Elements"
msgstr "\\- Element"

#: ../../reference_manual/clones_array.rst:20
msgid ""
"The amount of elements that should be generated using a negative of the "
"offset."
msgstr "Antal element som ska genereras med användning av negativ position."

#: ../../reference_manual/clones_array.rst:21
msgid "\\+ Elements"
msgstr "\\+ Element"

#: ../../reference_manual/clones_array.rst:22
msgid ""
"The amount of elements that should be generated using a positive of the "
"offset."
msgstr "Antal element som ska genereras med användning av positiv position."

#: ../../reference_manual/clones_array.rst:23
msgid "X offset"
msgstr "X-position"

#: ../../reference_manual/clones_array.rst:24
msgid ""
"The X offset in pixels. Use this in combination with Y offset to position a "
"clone using Cartesian coordinates."
msgstr ""
"X-positionen i bildpunkter. Använd den tillsammans med Y-positionen för att "
"placera en dubblett med kartesiska koordinater."

#: ../../reference_manual/clones_array.rst:25
msgid "Y offset"
msgstr "Y-position"

#: ../../reference_manual/clones_array.rst:26
msgid ""
"The Y offset in pixels. Use this in combination with X offset to position a "
"clone using Cartesian coordinates."
msgstr ""
"Y-positionen i bildpunkter. Använd den tillsammans med X-positionen för att "
"placera en dubblett med kartesiska koordinater."

#: ../../reference_manual/clones_array.rst:27
msgid "Distance"
msgstr "Avstånd"

#: ../../reference_manual/clones_array.rst:28
msgid ""
"The line-distance of the original origin to the clones origin. Use this in "
"combination with angle to position a clone using a polar coordinate system."
msgstr ""
"Det linjära avståndet av ursprungligt origo till dubbletternas origo. Använd "
"det tillsammans med vinkeln för att placera en dubblett med användning av "
"polära koordinater."

#: ../../reference_manual/clones_array.rst:30
msgid ""
"The angle-offset of the column or row. Use this in combination with distance "
"to position a clone using a polar coordinate system."
msgstr ""
"Vinkelposition av kolumnen eller raden. Använd det tillsammans med avståndet "
"för att placera en dubblett med användning av polära koordinater."
