# Translation of docs_krita_org_user_manual___drawing_tablets.po to Catalan
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2019.
# Josep Ma. Ferrer <txemaq@gmail.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: user_manual\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-05-12 13:09+0100\n"
"Last-Translator: Josep Ma. Ferrer <txemaq@gmail.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 2.0\n"

#: ../../user_manual/drawing_tablets.rst:0
msgid ".. image:: images/Krita_tablet_stylus.png"
msgstr ".. image:: images/Krita_tablet_stylus.png"

#: ../../user_manual/drawing_tablets.rst:1
msgid ""
"Basic page describing drawing tablets, how to set them up for Krita and how "
"to troubleshoot common tablet issues."
msgstr ""
"Pàgina bàsica que descriu el dibuixar en les tauletes, com configurar-les "
"per al Krita i com resoldre els problemes habituals en la tauleta."

#: ../../user_manual/drawing_tablets.rst:13
msgid "Tablets"
msgstr "Tauletes"

#: ../../user_manual/drawing_tablets.rst:18
msgid "Drawing Tablets"
msgstr "Dibuixar en tauletes"

#: ../../user_manual/drawing_tablets.rst:20
msgid ""
"This page is about drawing tablets, what they are, how they work, and where "
"things can go wrong."
msgstr ""
"Aquesta pàgina tracta sobre dibuixar en les tauletes, què són, com treballar-"
"hi i on poden anar malament les coses."

#: ../../user_manual/drawing_tablets.rst:24
msgid "What are tablets?"
msgstr "Què són les tauletes?"

#: ../../user_manual/drawing_tablets.rst:26
msgid ""
"Drawing with a mouse can be unintuitive and difficult compared to pencil and "
"paper. Even worse, extended mouse use can result in carpal tunnel syndrome. "
"That’s why most people who draw digitally use a specialized piece of "
"hardware known as a drawing tablet."
msgstr ""
"El dibuix amb un ratolí pot ser poc intuïtiu i difícil en comparació amb el "
"llapis i el paper. Encara pitjor, l'ús estès del ratolí pot provocar la "
"síndrome del canal carpià. Per això, la majoria de les persones que dibuixen "
"digitalment utilitzen una peça especialitzada de maquinari coneguda com a "
"tauleta de dibuix."

#: ../../user_manual/drawing_tablets.rst:32
msgid ".. image:: images/Krita_tablet_types.png"
msgstr ".. image:: images/Krita_tablet_types.png"

#: ../../user_manual/drawing_tablets.rst:33
msgid ""
"A drawing tablet is a piece of hardware that you can plug into your machine, "
"much like a keyboard or mouse. It usually looks like a plastic pad, with a "
"stylus. Another popular format is a computer monitor with stylus used to "
"draw directly on the screen. These are better to use than a mouse because "
"it’s more natural to draw with a stylus and generally better for your wrists."
msgstr ""
"Una tauleta de dibuix és una peça de maquinari que podeu connectar amb la "
"vostra màquina, com un teclat o un ratolí. Normalment sembla un coixí de "
"plàstic, amb un llapis. Un altre format popular és el monitor d'ordinador "
"amb llapis per a dibuixar directament a la pantalla. És millor que utilitzar "
"un ratolí, ja que és més natural dibuixar amb un llapis i, en general, "
"millor per als vostres canells."

#: ../../user_manual/drawing_tablets.rst:40
msgid ""
"With a properly installed tablet stylus, Krita can use information like "
"pressure sensitivity, allowing you to make strokes that get bigger or "
"smaller depending on the pressure you put on them, to create richer and more "
"interesting strokes."
msgstr ""
"Amb una placa de tauleta amb llapis correctament instal·lada, el Krita podrà "
"utilitzar la informació com la sensibilitat a la pressió, el qual us "
"permetrà fer traços cada cop més grans o petits, depenent de la pressió que "
"feu, per a crear traços més rics i interessants."

#: ../../user_manual/drawing_tablets.rst:46
msgid ""
"Sometimes, people confuse finger-touch styluses with a proper tablet. You "
"can tell the difference because a drawing tablet stylus usually has a pointy "
"nib, while a stylus made for finger-touch has a big rubbery round nib, like "
"a finger. These tablets may not give good results and a pressure-sensitive "
"tablet is recommended."
msgstr ""
"De vegades, les persones confonen els estils de tocar amb els dits amb una "
"tauleta adequada. Podeu dir-los-hi la diferència, ja que una tauleta de "
"dibuix amb llapis normalment té un punta punxeguda, mentre que un llapis "
"dissenyat per al contacte amb el dit té una gran punta de goma rodona, com "
"un dit. És possible que aquestes tauletes no donin bons resultats i es "
"recomana una tauleta sensible a la pressió."

#: ../../user_manual/drawing_tablets.rst:51
msgid "Drivers and Pressure Sensitivity"
msgstr "Controladors i sensibilitat a la pressió"

#: ../../user_manual/drawing_tablets.rst:53
msgid ""
"So you have bought a tablet, a real drawing tablet. And you wanna get it to "
"work with Krita! So you plug in the USB cable, start up Krita and... It "
"doesn’t work! Or well, you can make strokes, but that pressure sensitivity "
"you heard so much about doesn’t seem to work."
msgstr ""
"Així que heu comprat una tauleta, una veritable tauleta de dibuix. I voleu "
"que funcioni amb el Krita! Així doncs, connecteu el cable USB, inicieu el "
"Krita i... No funciona! O bé, podeu fer traços, però la sensibilitat a la "
"pressió de la que tant heu sentit parlar no sembla funcionar."

#: ../../user_manual/drawing_tablets.rst:58
msgid ""
"This is because you need to install a program called a ‘driver’. Usually you "
"can find the driver on a CD that was delivered alongside your tablet, or on "
"the website of the manufacturer. Go install it, and while you wait, we’ll go "
"into the details of what it is!"
msgstr ""
"Això es deu a que necessiteu instal·lar un programa anomenat «controlador». "
"Normalment, El trobareu en un CD lliurat al costat de la tauleta o al lloc "
"web del fabricant. Instal·leu-lo i, mentre espereu, anirem als detalls del "
"que és!"

#: ../../user_manual/drawing_tablets.rst:63
msgid ""
"Running on your computer is a basic system doing all the tricky bits of "
"running a computer for you. This is the operating system, or OS. Most people "
"use an operating system called Windows, but people on an Apple device have "
"an operating system called MacOS, and some people, including many of the "
"developers use a system called Linux."
msgstr ""
"S'executa en el vostre ordinador, es tracta d'un sistema bàsic que fa per "
"vosaltres totes les coses difícils d'utilitzar un ordinador. Aquest és el "
"sistema operatiu o SO. La majoria de la gent utilitza un sistema operatiu "
"anomenat Windows, però la gent d'un dispositiu Apple té un sistema operatiu "
"anomenat MacOS, i algunes persones, incloent-hi molts dels desenvolupadors, "
"utilitzen un sistema anomenat Linux."

#: ../../user_manual/drawing_tablets.rst:69
msgid ""
"The base principle of all of these systems is the same though. You would "
"like to run programs like Krita, called software, on your computer, and you "
"want Krita to be able to communicate with the hardware, like your drawing "
"tablet. But to have those two communicate can be really difficult - so the "
"operating system, works as a glue between the two."
msgstr ""
"El principi base de tots aquests sistemes és el mateix. Voleu executar "
"programes com el Krita, el qual s'anomena programari, al vostre ordinador, i "
"voleu que el Krita pugui comunicar-se amb el maquinari, com la vostra "
"tauleta de dibuix. Però tenir aquestes dues comunicacions pot ser realment "
"difícil, de manera que el sistema operatiu funciona com una cola entre els "
"dos."

#: ../../user_manual/drawing_tablets.rst:75
msgid ""
"Whenever you start Krita, Krita will first make connections with the "
"operating system, so it can ask it for a lot of these things: It would like "
"to display things, and use the memory, and so on. Most importantly, it would "
"like to get information from the tablet!"
msgstr ""
"Sempre que inicieu el Krita, primer es crearan les connexions amb el sistema "
"operatiu, de manera que podrà demanar per a un munt d'aquestes coses: Vol "
"mostrar les coses, emprar la memòria i així successivament. El més important "
"és que voldrà obtenir la informació de la tauleta!"

#: ../../user_manual/drawing_tablets.rst:81
msgid ".. image:: images/Krita_tablet_drivermissing.png"
msgstr ".. image:: images/Krita_tablet_drivermissing.png"

#: ../../user_manual/drawing_tablets.rst:82
msgid ""
"But it can’t! Turns out your operating system doesn’t know much about "
"tablets. That’s what drivers are for. Installing a driver gives the "
"operating system enough information so the OS can provide Krita with the "
"right information about the tablet. The hardware manufacturer's job is to "
"write a proper driver for each operating system."
msgstr ""
"Però no pot! Resulta que el vostre sistema operatiu no sap gaire sobre les- "
"tauletes. Per això estan els controladors. Instal·lar un controlador "
"proporciona al sistema operatiu prou informació perquè el sistema operatiu "
"pugui proporcionar al Krita la informació adequada sobre la tauleta. El "
"treball del fabricant de maquinari és escriure un controlador adequat per a "
"cada sistema operatiu."

#: ../../user_manual/drawing_tablets.rst:89
msgid ""
"Because drivers modify the operating system a little, you will always need "
"to restart your computer when installing or deinstalling a driver, so don’t "
"forget to do this! Conversely, because Krita isn’t a driver, you don’t need "
"to even deinstall it to reset the configuration, just rename or delete the "
"configuration file."
msgstr ""
"Com que els controladors modifiquen una mica el sistema operatiu, sempre "
"haureu de reiniciar l'ordinador quan n'instal·leu o desinstal·leu un, de "
"manera que no us oblideu de fer-ho! Per contra, perquè el Krita no és un "
"controlador, no us caldrà desinstal·lar-lo per a restablir la configuració, "
"simplement reanomeneu o suprimiu el fitxer de configuració."

#: ../../user_manual/drawing_tablets.rst:92
msgid "Where it can go wrong: Windows"
msgstr "On pot anar malament: Windows"

#: ../../user_manual/drawing_tablets.rst:94
msgid ""
"Krita automatically connects to your tablet if the drivers are installed. "
"When things go wrong, usually the problem isn't with Krita."
msgstr ""
"El Krita es connectarà automàticament amb la tauleta si els controladors "
"estan instal·lats. Quan les coses van malament, normalment el problema no és "
"amb el Krita."

#: ../../user_manual/drawing_tablets.rst:98
msgid "Surface pro tablets need two drivers"
msgstr "Les tauletes Surface Pro necessiten dos controladors"

#: ../../user_manual/drawing_tablets.rst:100
msgid ""
"Certain tablets using n-trig, like the Surface Pro, have two types of "
"drivers. One is native, n-trig and the other one is called wintab. Since "
"3.3, Krita can use Windows Ink style drivers, just go to :menuselection:"
"`Settings --> Configure Krita --> Tablet Settings` and toggle the :guilabel:"
"`Windows 8+ Pointer Input` there. You don't need to install the wintab "
"drivers anymore for n-trig based pens."
msgstr ""
"Algunes tauletes que utilitzen «n-trig», com la Surface Pro, tenen dos tipus "
"de controladors. Un és natiu, «n-trig» i l'altre s'anomena «wintab». Des de "
"la versió 3.3, el Krita pot utilitzar controladors per a l'estil de tinta "
"del Windows, només cal anar a :menuselection:`Arranjament --> Configura el "
"Krita --> Ajustaments per a la tauleta` i canvieu l'opció :guilabel:`Entrada "
"de l'apuntador de Windows 8+`. No necessiteu instal·lar més els controladors "
"«wintab» per a llapis basats en «n-trig»."

#: ../../user_manual/drawing_tablets.rst:108
msgid "Windows 10 updates"
msgstr "Actualitzacions del Windows 10"

#: ../../user_manual/drawing_tablets.rst:110
msgid ""
"Sometimes a Windows 10 update can mess up tablet drivers. In that case, "
"reinstalling the drivers should work."
msgstr ""
"De vegades, una actualització de Windows 10 pot espatllar els controladors "
"de les tauletes. En aquest cas, el reinstal·lar els controladors sol "
"funcionar."

#: ../../user_manual/drawing_tablets.rst:114
msgid "Wacom Tablets"
msgstr "Tauletes Wacom"

#: ../../user_manual/drawing_tablets.rst:116
msgid "There are two known problems with Wacom tablets and Windows."
msgstr "Hi ha dos problemes coneguts amb les tauletes Wacom i el Windows."

#: ../../user_manual/drawing_tablets.rst:118
msgid ""
"The first is that if you have customized the driver settings, then "
"sometimes, often after a driver update, but that is not necessary, the "
"driver breaks. Resetting the driver to the default settings and then loading "
"your settings from a backup will solve this problem."
msgstr ""
"El primer és que si heu personalitzat els ajustaments del controlador, de "
"vegades, sovint després d'una actualització del controlador, però això no és "
"necessari, el controlador es trenca. El restablir el controlador als "
"ajustaments predeterminats i després carregar els vostres ajustaments des "
"d'una còpia de seguretat solucionarà aquest problema."

#: ../../user_manual/drawing_tablets.rst:123
msgid ""
"The second is that for some reason it might be necessary to change the "
"display priority order. You might have to make your Cintiq screen your "
"primary screen, or, on the other hand, make it the secondary screen. Double "
"check in the Wacom settings utility that the tablet in the Cintiq is "
"associated with the Cintiq screen."
msgstr ""
"El segon és que, per alguna raó, podria ser necessari canviar l'ordre de "
"prioritat de mostrar. És possible que hagueu de fer que la vostra pantalla "
"de «Cintiq» sigui la pantalla principal o, d'altra banda, convertir-la en la "
"pantalla secundària. Comproveu a la utilitat d'ajustaments de Wacom que la "
"tauleta de «Cintiq» estigui associada amb la pantalla de «Cintiq»."

#: ../../user_manual/drawing_tablets.rst:130
msgid "Broken Drivers"
msgstr "Controladors trencats"

#: ../../user_manual/drawing_tablets.rst:132
msgid ""
"Tablet drivers need to be made by the manufacturer. Sometimes, with really "
"cheap tablets, the hardware is fine, but the driver is badly written, which "
"means that the driver just doesn’t work well. We cannot do anything about "
"this, sadly. You will have to send a complaint to the manufacturer for this, "
"or buy a better tablet with better quality drivers."
msgstr ""
"Els controladors de la tauleta han d'estar fabricats pel fabricant. De "
"vegades, amb tauletes realment barates, el maquinari està bé, però el "
"controlador està escrit malament, el qual vol dir que el controlador "
"simplement no funcionarà bé. Lamentablement no podem fer res referent a "
"això. Haureu d'enviar una queixa al fabricant, o comprar una tauleta millor "
"amb controladors de millor qualitat."

#: ../../user_manual/drawing_tablets.rst:140
msgid "Conflicting Drivers"
msgstr "Controladors conflictius"

#: ../../user_manual/drawing_tablets.rst:142
msgid ""
"On Windows, you can only have a single wintab-style driver installed at a "
"time. So be sure to deinstall the previous driver before installing the one "
"that comes with the tablet you want to use. Other operating systems are a "
"bit better about this, but even Linux, where the drivers are often "
"preinstalled, can't run two tablets with different drivers at once."
msgstr ""
"A Windows, només podreu tenir instal·lat alhora un únic controlador d'estil "
"«wintab». Així que assegureu-vos de desinstal·lar el controlador antic abans "
"d'instal·lar el que ve amb la tauleta que voleu utilitzar. Altres sistemes "
"operatius són una mica millors referent a això, però fins i tot Linux, on "
"els controladors sovint estan preinstal·lats, no es poden executar dues "
"tauletes amb diferents controladors alhora."

#: ../../user_manual/drawing_tablets.rst:150
msgid "Interfering software"
msgstr "Programari que interfereix"

#: ../../user_manual/drawing_tablets.rst:152
msgid ""
"Sometimes, there's software that tries to make a security layer between "
"Krita and the operating system. Sandboxie is an example of this. However, "
"Krita cannot always connect to certain parts of the operating system while "
"sandboxed, so it will often break in programs like sandboxie. Similarly, "
"certain mouse software, like Razer utilities can also affect whether Krita "
"can talk to the operating system, converting tablet information to mouse "
"information. This type of software should be configured to leave Krita "
"alone, or be deinstalled."
msgstr ""
"De vegades, hi ha programari que intenta crear una capa de seguretat entre "
"el Krita i el sistema operatiu. El Sandboxie n'és un exemple. No obstant "
"això, el Krita no sempre podrà connectar-se amb certes parts del sistema "
"operatiu mentre es trobi en un espai aïllat, de manera que sovint "
"s'interromp front a programes com el Sandboxie. De manera similar, algun "
"programari de ratolí, com les utilitats de Razer, també pot afectar en si el "
"Krita podrà parlar amb el sistema operatiu, convertint la informació de la "
"tauleta en informació del ratolí. Aquest tipus de programari s'haurà de "
"configurar o desinstal·lar per a deixar que el Krita faci sol."

#: ../../user_manual/drawing_tablets.rst:161
msgid ""
"The following software has been reported to interfere with tablet events to "
"Krita:"
msgstr ""
"S'ha informat que el següent programari interfereix el Krita amb els "
"esdeveniments de la tauleta:"

#: ../../user_manual/drawing_tablets.rst:164
msgid "Sandboxie"
msgstr "Sandboxie"

#: ../../user_manual/drawing_tablets.rst:165
msgid "Razer mouse utilities"
msgstr "Utilitats del ratolí de Razer"

#: ../../user_manual/drawing_tablets.rst:166
msgid "AMD catalyst “game mode” (this broke the right click for someone)"
msgstr "«Mode joc» de l'«AMD catalyst» (aquest va trencar el clic dret a algú)"

#: ../../user_manual/drawing_tablets.rst:169
msgid "Flicks (Wait circle showing up and then calling the popup palette)"
msgstr ""
"Flicks (espera el cercle que apareix i després crida la paleta emergent)"

#: ../../user_manual/drawing_tablets.rst:171
msgid ""
"If you have a situation where trying to draw keeps bringing up the pop-up "
"palette on Windows, then the problem might be flicks. These are a type of "
"gesture, a bit of Windows functionality that allows you to make a motion to "
"serve as a keyboard shortcut. Windows automatically turns these on when you "
"install tablet drivers, because the people who made this part of Windows "
"forgot that people also draw with computers. So you will need to turn it off "
"in the Windows flicks configuration."
msgstr ""
"Si us trobeu amb una situació en la que intentant dibuixar es continua "
"mostrant la paleta emergent al Windows, llavors el problema pot ser el "
"«Flicks». Aquests són un tipus de gest, una mica de funcionalitat del "
"Windows que permet fer un moviment de manera que serveixi com a drecera del "
"teclat. El Windows els activa de manera automàtica quan instal·la els "
"controladors de la tauleta, perquè les persones que van fer aquesta part del "
"Windows van oblidar que la gent també dibuixa amb els ordinadors. Per tant, "
"haureu de desactivar-lo a la configuració del «Flicks» de Windows."

#: ../../user_manual/drawing_tablets.rst:180
msgid "Wacom Double Click Sensitivity (Straight starts of lines)"
msgstr "Sensibilitat del doble clic de Wacom (l'inici recte de les línies)"

#: ../../user_manual/drawing_tablets.rst:182
msgid ""
"If you experience an issue where the start of the stroke is straight, and "
"have a wacom tablet, it could be caused by the Wacom driver double-click "
"detection."
msgstr ""
"Si experimenteu un problema en el que l'inici del traç és recte i teniu una "
"tauleta de Wacom, pot estar provocat per la detecció del doble clic del "
"controlador de Wacom."

#: ../../user_manual/drawing_tablets.rst:186
msgid ""
"To fix this, go to the Wacom settings utility and lower the double click "
"sensitivity."
msgstr ""
"Per a solucionar aquest problema, aneu a la utilitat d'ajustaments de Wacom "
"i baixeu la sensibilitat al doble clic."

#: ../../user_manual/drawing_tablets.rst:190
msgid "Supported Tablets"
msgstr "Tauletes acceptades"

#: ../../user_manual/drawing_tablets.rst:192
msgid ""
"Supported tablets are the ones of which Krita developers have a version "
"themselves, so they can reliably fix bugs with them. :ref:`We maintain a "
"list of those here <list_supported_tablets>`."
msgstr ""
"Les tauletes acceptades són aquelles en les que els desenvolupadors del "
"Krita tenen una versió, de manera que poden corregir els errors de forma "
"fiable. :ref:`Aquí mantenim una llista de les que són "
"<list_supported_tablets>`."
