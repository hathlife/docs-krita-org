# translation of docs_krita_org_reference_manual___dockers___reference_images_docker.po to Slovak
# Roman Paholik <wizzardsk@gmail.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: "
"docs_krita_org_reference_manual___dockers___reference_images_docker\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-04-02 10:07+0200\n"
"Last-Translator: Roman Paholik <wizzardsk@gmail.com>\n"
"Language-Team: Slovak <kde-sk@linux.sk>\n"
"Language: sk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 18.12.3\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"

#: ../../reference_manual/dockers/reference_images_docker.rst:1
msgid "Overview of the pattern docker."
msgstr ""

#: ../../reference_manual/dockers/reference_images_docker.rst:12
msgid "Reference"
msgstr ""

#: ../../reference_manual/dockers/reference_images_docker.rst:17
msgid "Reference Images Docker"
msgstr ""

#: ../../reference_manual/dockers/reference_images_docker.rst:21
msgid ""
"This docker was removed in Krita 4.0 due to crashes on Windows. :ref:`The "
"reference images tool in 4.1 replaces it. <reference_images_tool>`"
msgstr ""

#: ../../reference_manual/dockers/reference_images_docker.rst:24
#, fuzzy
#| msgid ".. image:: images/en/400px-Krita_Reference_Images_Browse_Docker.png"
msgid ""
".. image:: images/dockers/400px-Krita_Reference_Images_Browse_Docker.png"
msgstr ".. image:: images/en/400px-Krita_Reference_Images_Browse_Docker.png"

#: ../../reference_manual/dockers/reference_images_docker.rst:26
#, fuzzy
#| msgid ".. image:: images/en/400px-Krita_Reference_Images_Image_Docker.png"
msgid ".. image:: images/dockers/400px-Krita_Reference_Images_Image_Docker.png"
msgstr ".. image:: images/en/400px-Krita_Reference_Images_Image_Docker.png"

#: ../../reference_manual/dockers/reference_images_docker.rst:27
msgid ""
"This docker allows you to pick an image from outside of Krita and use it as "
"a reference. Even better, you can pick colors from it directly."
msgstr ""

#: ../../reference_manual/dockers/reference_images_docker.rst:29
msgid "The docker consists of two tabs: Browsing and Image."
msgstr ""

#: ../../reference_manual/dockers/reference_images_docker.rst:32
msgid "Browsing"
msgstr "Prehliadanie"

#: ../../reference_manual/dockers/reference_images_docker.rst:34
msgid ""
"Browsing gives you a small file browser, so you can navigate to the map "
"where the image you want to use as reference is located."
msgstr ""

#: ../../reference_manual/dockers/reference_images_docker.rst:36
msgid ""
"There's an image strip beneath the browser, allowing you to select the image "
"which you want to use. Double click to load it in the :guilabel:`Image` tab."
msgstr ""

#: ../../reference_manual/dockers/reference_images_docker.rst:39
msgid "Image"
msgstr "Obrázok"

#: ../../reference_manual/dockers/reference_images_docker.rst:41
msgid ""
"This tab allows you to see the images you selected, and change the zoom "
"level. Clicking anywhere on the image will allow you to pick the merged "
"color from it. Using the cross symbol, you can remove the icon."
msgstr ""
