# Translation of docs_krita_org_user_manual___soft_proofing.po to Catalan
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: user_manual\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-02 03:06+0200\n"
"PO-Revision-Date: 2019-08-02 19:04+0200\n"
"Last-Translator: Antoni Bella Pérez <antonibella5@yahoo.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.07.70\n"

#: ../../user_manual/soft_proofing.rst:0
msgid ".. image:: images/softproofing/Softproofing_adaptationstate.png"
msgstr ".. image:: images/softproofing/Softproofing_adaptationstate.png"

#: ../../user_manual/soft_proofing.rst:1
msgid "How to use softproofing in Krita."
msgstr "Com utilitzar la provatura suau en el Krita."

#: ../../user_manual/soft_proofing.rst:11
msgid "Color"
msgstr "Color"

#: ../../user_manual/soft_proofing.rst:11
msgid "Softproofing"
msgstr "Provatura suau"

#: ../../user_manual/soft_proofing.rst:16
msgid "Soft Proofing"
msgstr "Provatura suau"

#: ../../user_manual/soft_proofing.rst:18
msgid ""
"When we make an image in Krita, and print that out with a printer, the image "
"tends to look different. The colors are darker, or less dark than expected, "
"maybe the reds are more aggressive, maybe contrast is lost. For simple "
"documents, this isn’t much of a problem, but for professional prints, this "
"can be very sad, as it can change the look and feel of an image drastically."
msgstr ""
"Quan creem una imatge en el Krita i l'imprimim amb una impressora, la imatge "
"tendeix a veure's diferent. Els colors són més o menys foscos del que "
"s'esperava, potser els vermells són més agressius, potser s'ha perdut el "
"contrast. Per a documents senzills, això no és un gran problema, però per a "
"impressions professionals, això pot ser molt trist, ja que pot canviar "
"dràsticament l'aspecte d'una imatge."

#: ../../user_manual/soft_proofing.rst:20
msgid ""
"The reason this happens is simply because the printer uses a different color "
"model (CMYK) and it has often access to a lower range of colors (called a "
"gamut)."
msgstr ""
"La raó per la qual això passa és simplement perquè la impressora utilitza un "
"model de color diferent (CMYK) i, sovint, té accés a una gamma de colors més "
"baixa (anomenada «gamut» o en la traducció gamma)."

#: ../../user_manual/soft_proofing.rst:22
msgid ""
"A naive person would suggest the following solution: do your work within the "
"CMYK color model! But there are three problems with that:"
msgstr ""
"Una persona ingènua suggeriria la següent solució: feu el vostre treball "
"dins el model de color CMYK! Però hi ha tres problemes amb això:"

#: ../../user_manual/soft_proofing.rst:24
msgid ""
"Painting in a CMYK space doesn’t guarantee that the colors will be the same "
"on your printer. For each combination of Ink, Paper and Printing device, the "
"resulting gamut of colors you can use is different. Which means that each of "
"these could have a different profile associated with them."
msgstr ""
"Pintar en un espai CMYK no garanteix que els colors siguin els mateixos a la "
"impressora. Per a cada combinació de tinta, paper i dispositiu d'impressió, "
"la gamma de colors resultant que podreu utilitzar serà diferent. El qual vol "
"dir que cadascun d'aquests podria tenir un perfil diferent associat."

#: ../../user_manual/soft_proofing.rst:25
msgid ""
"Furthermore, even if you have the profile and are working in the exact color "
"space that your printer can output, the CMYK color space is very irregular, "
"meaning that the color maths isn’t as nice as in other spaces. Blending "
"modes are different in CMYK as well."
msgstr ""
"A més, fins i tot si teniu el perfil i esteu treballant en l'espai de color "
"exacte que pot imprimir la impressora, l'espai de color CMYK és molt "
"irregular, el qual vol dir que les matemàtiques del color no són tan "
"agradables com en altres espais. Els modes de barreja també són diferents en "
"el CMYK."

#: ../../user_manual/soft_proofing.rst:26
msgid ""
"Finally, working in that specific CMYK space means that the image is stuck "
"to that space. If you are preparing your work for  different a CMYK profile, "
"due to the paper, printer or ink being different, you might have a bigger "
"gamut with more bright colors that you would like to take advantage of."
msgstr ""
"Finalment, treballar en aquest espai CMYK específic voldrà dir que la imatge "
"estarà encallada a aquest espai. Si esteu preparant el vostre treball per a "
"un perfil CMYK diferent, a causa que el paper, la impressora o la tinta són "
"diferents, és possible que tingueu una gamma més àmplia amb colors més "
"brillants que us agradaria aprofitar."

#: ../../user_manual/soft_proofing.rst:28
msgid ""
"So ideally, you would do the image in RGB, and use all your favorite RGB "
"tools, and let the computer do a conversion to a given CMYK space on the "
"fly, just for preview. This is possible, and is what we call ''Soft "
"Proofing''."
msgstr ""
"Per tant, l'ideal seria fer la imatge en RGB i utilitzar totes les vostres "
"eines RGB preferides, i deixar que l'ordinador faci una conversió a un espai "
"CMYK determinat sobre la marxa, només per a la vista prèvia. Això és "
"possible, i és el que anomenem «Provatura suau»."

#: ../../user_manual/soft_proofing.rst:34
msgid ".. image:: images/softproofing/Softproofing_regularsoftproof.png"
msgstr ".. image:: images/softproofing/Softproofing_regularsoftproof.png"

#: ../../user_manual/soft_proofing.rst:34
msgid ""
"On the left, the original, on the right, a view where soft proofing is "
"turned on. The difference is subtle due to the lack of really bright colors, "
"but the soft proofed version is slightly less blueish in the whites of the "
"flowers and slightly less saturated in the greens of the leaves."
msgstr ""
"A l'esquerra, l'original, a la dreta, una vista on s'activa la provatura "
"suau. La diferència és subtil, causa de la falta de colors realment "
"brillants, però la versió de la provatura suau és lleugerament menys blavosa "
"en els blancs de les flors i lleugerament menys saturada en els verds de les "
"fulles."

#: ../../user_manual/soft_proofing.rst:36
msgid ""
"You can toggle soft proofing on any image using the :kbd:`Ctrl + Y` "
"shortcut. Unlike other programs, this is per-view, so that you can look at "
"your image non-proofed and proofed, side by side. The settings are also per "
"image, and saved into the .kra file. You can set the proofing options in :"
"menuselection:`Image --> Image Properties --> Soft Proofing`."
msgstr ""
"Podeu alternar la provatura suau sobre qualsevol imatge utilitzant la "
"drecera :kbd:`Ctrl + Y`. A diferència d'altres programes, això és per la "
"vista, de manera que podreu mirar la vostra imatge amb i sense la provatura, "
"costat a costat. Els ajustaments també són per imatge i es desen al fitxer «."
"kra». Podeu establir les opcions de la provatura a :menuselection:`Imatge --"
"> Propietats de la imatge --> Provatura suau`."

#: ../../user_manual/soft_proofing.rst:38
msgid "There you can set the following options:"
msgstr "Allà podreu establir les següents opcions:"

#: ../../user_manual/soft_proofing.rst:40
msgid "Profile, Depth, Space"
msgstr "Perfil, Profunditat, Espai"

#: ../../user_manual/soft_proofing.rst:41
msgid ""
"Of these, only the profile is really important. This will serve as the "
"profile you are proofing to. In a professional print workflow, this profile "
"should be determined by the printing house."
msgstr ""
"D'aquestes, només el perfil és realment important. Servirà com el perfil amb "
"el qual esteu realitzant la provatura. En un flux de treball d'impressió "
"professional, aquest perfil haurà de ser determinat per la impremta."

#: ../../user_manual/soft_proofing.rst:43
msgid ""
"Set the proofing Intent. It uses the same intents as the intents mentioned "
"in the :ref:`color managed workflow <color_managed_workflow>`."
msgstr ""
"Estableix el propòsit de la provatura. Utilitza els mateixos propòsits que "
"els esmentats durant el :ref:`flux de treball en la gestió del color "
"<color_managed_workflow>`."

#: ../../user_manual/soft_proofing.rst:48
msgid "Intent"
msgstr "Propòsit"

#: ../../user_manual/soft_proofing.rst:49
msgid ""
"Left: Soft proofed image with Adaptation state slider set to max. Right: "
"Soft proofed image with Adaptation State set to minimum"
msgstr ""
"Esquerra: imatge amb provatura suau amb el control lliscant Estat de "
"l'adaptació establert al màxim. Dreta: imatge amb provatura suau amb Estat "
"de l'adaptació establert al mínim."

#: ../../user_manual/soft_proofing.rst:50
msgid "Adaptation State"
msgstr "Estat de l'adaptació"

#: ../../user_manual/soft_proofing.rst:51
msgid ""
"A feature which allows you to set whether :guilabel:`Absolute Colorimetric` "
"will make the white in the image screen-white during proofing (the slider "
"set to max), or whether it will use the white point of the profile (the "
"slider set to minimum). Often CMYK profiles have a different white as the "
"screen, or amongst one another due to the paper color being different."
msgstr ""
"Una característica que permet establir si :guilabel:`Colorimetria absoluta` "
"farà que el blanc a la pantalla de la imatge sigui blanc durant la provatura "
"(el control lliscant s'establirà al màxim), o si utilitzarà el punt blanc "
"del perfil (el control lliscant s'establirà al mínim). Sovint, els perfils "
"CMYK tenen un blanc diferent com la pantalla, o entre ells pel fet que el "
"color del paper és diferent."

#: ../../user_manual/soft_proofing.rst:52
msgid "Black Point Compensation"
msgstr "Compensació del punt negre"

#: ../../user_manual/soft_proofing.rst:53
msgid ""
"Set the black point compensation. Turning this off will crunch the shadow "
"values to the minimum the screen and the proofing profile can handle, while "
"turning this on will scale the black to the screen-range, showing you the "
"full range of grays in the image."
msgstr ""
"Estableix la compensació del punt negre. Si la desactiveu, els valors de "
"l'ombra es reduiran al mínim que la pantalla i el perfil de provatura puguin "
"manejar, mentre que en activar aquesta opció s'escalarà el negre dins de "
"l'interval de la pantalla, mostrant el rang complet de grisos a la imatge."

#: ../../user_manual/soft_proofing.rst:55
msgid "Gamut Warning"
msgstr "Avís de gamma"

#: ../../user_manual/soft_proofing.rst:55
msgid "Set the color of the out-of-gamut warning."
msgstr "Estableix el color de l'avís de fora de la gamma."

#: ../../user_manual/soft_proofing.rst:57
msgid ""
"You can set the defaults that Krita uses in :menuselection:`Settings --> "
"Configure Krita --> Color Management`."
msgstr ""
"Podeu establir els valors predeterminats que utilitzarà el Krita a :"
"menuselection:`Arranjament --> Configura el Krita --> Gestió del color`."

#: ../../user_manual/soft_proofing.rst:59
msgid ""
"To configure this properly, it's recommended to make a test image to print "
"(and that is printed by a properly set-up printer) and compare against, and "
"then approximate in the proofing options how the image looks compared to the "
"real-life copy you have made."
msgstr ""
"Per a configurar-ho correctament, es recomana crear una imatge de prova per "
"imprimir (i que s'imprimeixi en una impressora configurada correctament) i "
"comparar-la, i després apropar-se a les opcions de la provatura com es veu a "
"la imatge comparada amb la còpia real que heu fet."

#: ../../user_manual/soft_proofing.rst:62
msgid "Out of Gamut Warning"
msgstr "Avís de fora de la gamma"

#: ../../user_manual/soft_proofing.rst:64
msgid ""
"The out of gamut warning, or gamut alarm, is an extra option on top of Soft-"
"Proofing: It allows you to see which colors are being clipped, by replacing "
"the resulting color with the set alarm color."
msgstr ""
"L'avís de fora de la gamma, o l'alarma de la gamma, és una opció addicional "
"a la part superior de la Provatura suau: us permet veure quins colors es "
"retallen, en substituir el color resultant amb el color d'alarma establert."

#: ../../user_manual/soft_proofing.rst:66
msgid ""
"This can be useful to determine where certain contrasts are being lost, and "
"to allow you to change it slowly to a less contrasted image."
msgstr ""
"Això pot ser útil per a determinar on s'estan perdent certs contrastos i per "
"a permetre canviar lentament a una imatge amb menys contrast."

#: ../../user_manual/soft_proofing.rst:72
msgid ".. image:: images/softproofing/Softproofing_gamutwarnings.png"
msgstr ".. image:: images/softproofing/Softproofing_gamutwarnings.png"

#: ../../user_manual/soft_proofing.rst:72
msgid ""
"Left: View with original image, Right: View with soft proofing and gamut "
"warnings turned on. Krita will save the gamut warning color alongside the "
"proofing options into the Kra file, so pick a color that you think will "
"stand out for your current image."
msgstr ""
"Esquerra: vista amb la imatge original, Dreta: vista amb la provatura suau i "
"els avisos de gamma activats. El Krita desarà el color d'avís de la gamma "
"juntament amb les opcions de la provatura al fitxer «Kra», de manera que "
"trieu un color que cregueu que destaqui per a la vostra imatge actual."

#: ../../user_manual/soft_proofing.rst:74
msgid ""
"You can activate Gamut Warnings with the :kbd:`Ctrl + Shift + Y` shortcut, "
"but it needs soft proofing activated to work fully."
msgstr ""
"Podeu activar els avisos de la gamma amb la drecera :kbd:`Ctrl + Majús. + "
"Y`, però necessitareu que la provatura suau estigui activada per a que "
"funcioni completament."

#: ../../user_manual/soft_proofing.rst:77
msgid ""
"Soft Proofing doesn’t work properly in floating-point spaces, and attempting "
"to force it will cause incorrect gamut alarms. It is therefore disabled."
msgstr ""
"La provatura suau no funciona correctament en espais de coma flotant, i "
"intentar forçar-ho causarà alarmes de gamma incorrecta. Per tant està "
"inhabilitada."

# skip-rule: t-acc_obe
#: ../../user_manual/soft_proofing.rst:80
msgid ""
"Gamut Warnings sometimes give odd warnings for linear profiles in the "
"shadows. This is a bug in LCMS, see `here <https://ninedegreesbelow.com/bug-"
"reports/soft-proofing-problems.html>`_ for more info."
msgstr ""
"Els avisos de gamma de vegades donen advertències senars en les ombres dels "
"perfils lineals. Aquest és un error en el LCMS, vegeu `aquí <https://"
"ninedegreesbelow.com/bug-reports/soft-proofing-problems.html>`_ per a més "
"informació."
