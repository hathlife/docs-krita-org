# Translation of docs_krita_org_reference_manual___brushes___brush_settings___masked_brush.po to Ukrainian
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Yuri Chornoivan <yurchor@ukr.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: "
"docs_krita_org_reference_manual___brushes___brush_settings___masked_brush\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-09-01 03:12+0200\n"
"PO-Revision-Date: 2019-06-15 08:20+0300\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <kde-i18n-uk@kde.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 19.04.0\n"

#: ../../reference_manual/brushes/brush_settings/masked_brush.rst:0
msgid ".. image:: images/brushes/Masking-brush2.jpg"
msgstr ".. image:: images/brushes/Masking-brush2.jpg"

#: ../../reference_manual/brushes/brush_settings/masked_brush.rst:None
msgid ".. image:: images/brushes/Masking-brush1.jpg"
msgstr ".. image:: images/brushes/Masking-brush1.jpg"

#: ../../reference_manual/brushes/brush_settings/masked_brush.rst:1
msgid ""
"How to use the masked brush functionality in Krita. This functionality is "
"not unlike the dual brush option from photoshop."
msgstr ""
"Як користуватися можливостями пензля маскування у Krita. Ці можливості не "
"відрізняються від можливостей подвійного пензля у Photoshop."

#: ../../reference_manual/brushes/brush_settings/masked_brush.rst:13
#: ../../reference_manual/brushes/brush_settings/masked_brush.rst:18
msgid "Masked Brush"
msgstr "Пензель маскування"

#: ../../reference_manual/brushes/brush_settings/masked_brush.rst:13
msgid "Dual Brush"
msgstr "Дуальний пензель"

#: ../../reference_manual/brushes/brush_settings/masked_brush.rst:13
msgid "Stacked Brush"
msgstr "Стосовий пензель"

#: ../../reference_manual/brushes/brush_settings/masked_brush.rst:23
msgid ""
"Masked brush is new feature that is only available in the :ref:"
"`pixel_brush_engine`. They are additional settings you will see in the brush "
"editor. Masked brushes allow you to combine two brush tips in one stroke. "
"One brush tip will be a mask for your primary brush tip. A masked brush is a "
"good alternative to texture for creating expressive and textured brushes."
msgstr ""
"Пензель маскування є новою можливістю, яка доступна лише для інструмента :"
"ref:`pixel_brush_engine`. Реалізується можливість через додаткові параметри, "
"які буде показано у редакторі пензлів. Пензлі маскування надають вам змогу "
"поєднувати два кінчика пензля у одному мазку. Один із кінчиків пензлів буде "
"маскою для основного кінчика пензля. Пензель маскування є чудовою "
"альтернативою до текстури для створення виразних і текстурованих пензлів."

#: ../../reference_manual/brushes/brush_settings/masked_brush.rst:29
msgid ""
"Due to technological constraints, the masked brush only works in the wash "
"painting mode. However, do remember that flow works as opacity does in the "
"build-up painting mode."
msgstr ""
"Через технологічні обмеження пензель маскування працює лише у режимі "
"малювання аквареллю. Втім, слід пам'ятати, що у режимі надмальовування потік "
"працює як непрозорість."

#: ../../reference_manual/brushes/brush_settings/masked_brush.rst:32
msgid ""
"Like with normal brush tip you can choose any brush tip and change it size, "
"spacing, and rotation. Masking brush size is relative to main brush size. "
"This means when you change your brush size masking tip will be changed to "
"keep the ratio."
msgstr ""
"Як і зі звичайним кінчиком пензля, ви можете вибрати будь-який кінчик пензля "
"і змінити його розмір, інтервал та обертання. Розмір пензля маскування "
"задається відносно розміру основного пензля. Це означає, що коли ви змінюєте "
"розмір вашого пензля, розмір кінчика пензля маскування також змінюється зі "
"збереженням початкового співвідношення розмірів."

#: ../../reference_manual/brushes/brush_settings/masked_brush.rst:35
msgid ":ref:`Blending mode (drop-down inside Brush tip)<blending_modes>`:"
msgstr ""
":ref:`Режим змішування (спадний список у пункті кінчика "
"пензля)<blending_modes>`:"

#: ../../reference_manual/brushes/brush_settings/masked_brush.rst:35
msgid "Blending modes changes how tips are combined."
msgstr "Режими змішування змінюють характер поєднання кінчиків."

#: ../../reference_manual/brushes/brush_settings/masked_brush.rst:38
msgid ":ref:`option_brush_tip`"
msgstr ":ref:`option_brush_tip`"

#: ../../reference_manual/brushes/brush_settings/masked_brush.rst:40
msgid ":ref:`option_size`"
msgstr ":ref:`option_size`"

#: ../../reference_manual/brushes/brush_settings/masked_brush.rst:41
msgid "The size sensor option of the second tip."
msgstr "Параметр датчика розміру на другому кінчику."

#: ../../reference_manual/brushes/brush_settings/masked_brush.rst:42
msgid ":ref:`option_opacity_n_flow`"
msgstr ":ref:`option_opacity_n_flow`"

#: ../../reference_manual/brushes/brush_settings/masked_brush.rst:43
msgid ""
"The opacity and flow of the second tip. This is mapped to a sensor by "
"default. Flow can be quite aggressive on subtract mode, so it might be an "
"idea to turn it off there."
msgstr ""
"Непрозорість і потік другого кінчика пензля. Його, типово, буде пов'язано із "
"датчиком планшета. Потік може бути доволі агресивним у різницевому режимі, "
"тому зв'язок із датчиком, можливо, варто вимкнути."

#: ../../reference_manual/brushes/brush_settings/masked_brush.rst:44
msgid ":ref:`option_ratio`"
msgstr ":ref:`option_ratio`"

#: ../../reference_manual/brushes/brush_settings/masked_brush.rst:45
msgid "This affects the brush ratio on a given brush."
msgstr "Впливає на співвідношення розмірів пензля для вказаного пензля."

#: ../../reference_manual/brushes/brush_settings/masked_brush.rst:46
msgid ":ref:`option_mirror`"
msgstr ":ref:`option_mirror`"

#: ../../reference_manual/brushes/brush_settings/masked_brush.rst:47
msgid "The Mirror option of the second tip."
msgstr "Параметр віддзеркалення для другого кінчика."

#: ../../reference_manual/brushes/brush_settings/masked_brush.rst:48
msgid ":ref:`option_rotation`"
msgstr ":ref:`option_rotation`"

#: ../../reference_manual/brushes/brush_settings/masked_brush.rst:49
msgid "The rotation option of the second tip. Best set to \"fuzzy dab\"."
msgstr ""
"Параметр обертання другого кінчика. Найкращий варіант «Нечіткий мазок»."

#: ../../reference_manual/brushes/brush_settings/masked_brush.rst:51
msgid ":ref:`option_scatter`"
msgstr ":ref:`option_scatter`"

#: ../../reference_manual/brushes/brush_settings/masked_brush.rst:51
msgid ""
"The scatter option. The default is quite high, so don't forget to turn it "
"lower."
msgstr ""
"Параметр розсіювання. Типове значення є доволі високим, тому можете його "
"зменшити."

#: ../../reference_manual/brushes/brush_settings/masked_brush.rst:53
msgid "Difference from :ref:`option_texture`:"
msgstr "Відмінності від :ref:`option_texture`:"

#: ../../reference_manual/brushes/brush_settings/masked_brush.rst:55
msgid "You don’t need seamless texture to make cool looking brush"
msgstr ""
"Вам не потрібна безшовна текстура для створення чудового вигляду пензля."

#: ../../reference_manual/brushes/brush_settings/masked_brush.rst:56
msgid "Stroke generates on the fly, it always different"
msgstr "Мазок створюється на льоту і завжди є неповторним."

#: ../../reference_manual/brushes/brush_settings/masked_brush.rst:57
msgid "Brush strokes looks same on any brush size"
msgstr "Мазки пензлем виглядають однаково для усіх розмірів пензля."

#: ../../reference_manual/brushes/brush_settings/masked_brush.rst:58
msgid ""
"Easier to fill some areas with solid color but harder to make it hard "
"textured"
msgstr ""
"Простіше заповнювати певні ділянки суцільним кольором, але важче жорстко "
"текстурувати ділянку."

#~ msgid ""
#~ "You don’t need seamless texture to make cool looking brush Stroke "
#~ "generates on the fly, it always different Brush strokes looks same on any "
#~ "brush size Easier to fill some areas with solid color but harder to make "
#~ "it hard textured"
#~ msgstr ""
#~ "Вам не потрібна безшовна текстура для створення чудового вигляду пензля. "
#~ "Мазок створюється на льоту і завжди є неповторним. Мазки пензлем "
#~ "виглядають однаково для усіх розмірів пензля. Простіше заповнювати певні "
#~ "ділянки суцільним кольором, але важче жорстко текстурувати ділянку."
