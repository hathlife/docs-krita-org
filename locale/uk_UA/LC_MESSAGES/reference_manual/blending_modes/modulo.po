# Translation of docs_krita_org_reference_manual___blending_modes___modulo.po to Ukrainian
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Yuri Chornoivan <yurchor@ukr.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: "
"docs_krita_org_reference_manual___blending_modes___modulo\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-09 03:41+0200\n"
"PO-Revision-Date: 2019-05-09 07:33+0300\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <kde-i18n-uk@kde.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 19.03.70\n"

#: ../../reference_manual/blending_modes/modulo.rst:1
msgid "Page about the modulo blending modes in Krita:"
msgstr "Сторінка щодо режимів змішування за залишком у Krita:"

#: ../../reference_manual/blending_modes/modulo.rst:10
#: ../../reference_manual/blending_modes/modulo.rst:14
#: ../../reference_manual/blending_modes/modulo.rst:47
#: ../../reference_manual/blending_modes/modulo.rst:51
msgid "Modulo"
msgstr "Залишок"

#: ../../reference_manual/blending_modes/modulo.rst:16
msgid ""
"Modulo modes are special class of blending modes which loops values when the "
"value of channel blend layer is less than the value of channel in base "
"layers. All modes in modulo modes retains the absolute of the remainder if "
"the value is greater than the maximum value or the value is less than "
"minimum value. Continuous modes assume if the calculated value before modulo "
"operation is within the range between a odd number to even number, then "
"values are inverted in the end result, so values are perceived to be wave-"
"like."
msgstr ""
"Режими залишку є особливим класом режимів змішування, які використовують, "
"коли значення у каналі шару змішування є меншим за значення у каналі "
"основного шару. Усі режими залишку дають модуль лишку, якщо значення є "
"більшим за максимальне значення або значення є меншим за мінімальне "
"значення. У неперервних версіях режимів припускають, що, якщо обчислене "
"значення перед операцією визначення залишку потрапляє до проміжку між "
"непарним і парним числом, у кінцевій формулі до значень застосовують "
"інверсію. Це надає змогу отримати хвилеподібні результати без різких "
"переходів."

#: ../../reference_manual/blending_modes/modulo.rst:18
msgid ""
"Furthermore, this would imply that modulo modes are beneficial for abstract "
"art, and manipulation of gradients."
msgstr ""
"Крім того, з цього слідує, що режими залишків дають добрі результати для "
"абстрактних творів та роботи з градієнтами."

#: ../../reference_manual/blending_modes/modulo.rst:20
#: ../../reference_manual/blending_modes/modulo.rst:24
msgid "Divisive Modulo"
msgstr "Залишок від ділення"

#: ../../reference_manual/blending_modes/modulo.rst:26
msgid ""
"First, Base Layer is divided by the sum of blend layer and the minimum "
"possible value after zero. Then, performs a modulo calculation using the "
"value found with the sum of blend layer and the minimum possible value after "
"zero."
msgstr ""
"Спочатку, значення у шарі основи буде поділено на суму значення у шарі "
"змішування та мінімального можливого значення після нуля. Далі, буде "
"обчислено залишок за основою значення суми значення шару змішування та "
"мінімального можливого значення після нуля."

#: ../../reference_manual/blending_modes/modulo.rst:31
msgid ""
".. image:: images/blending_modes/modulo/"
"Blending_modes_Divisive_Modulo_Gradient_Comparison.png"
msgstr ""
".. image:: images/blending_modes/modulo/"
"Blending_modes_Divisive_Modulo_Gradient_Comparison.png"

#: ../../reference_manual/blending_modes/modulo.rst:31
msgid ""
"Left: **Base Layer**. Middle: **Blend Layer**. Right: **Divisive Modulo**."
msgstr ""
"Ліворуч: **Базовий шар**. Посередині: **Шар змішування**. Праворуч: "
"**Залишок від ділення**."

#: ../../reference_manual/blending_modes/modulo.rst:33
#: ../../reference_manual/blending_modes/modulo.rst:38
msgid "Divisive Modulo - Continuous"
msgstr "Залишок від ділення - неперервний"

#: ../../reference_manual/blending_modes/modulo.rst:40
msgid ""
"First, Base Layer is divided by the sum of blend layer and the minimum "
"possible value after zero. Then, performs a modulo calculation using the "
"value found with the sum of blend layer and the minimum possible value after "
"zero. As this is a continuous mode, anything between odd to even numbers are "
"inverted."
msgstr ""
"Спочатку, значення у шарі основи буде поділено на суму значення у шарі "
"змішування та мінімального можливого значення після нуля. Далі, буде "
"обчислено залишок за основою значення суми значення шару змішування та "
"мінімального можливого значення після нуля. Оскільки це неперервний режим, "
"усі значення між непарними і парними числами інвертуватимуться."

#: ../../reference_manual/blending_modes/modulo.rst:45
msgid ""
".. image:: images/blending_modes/modulo/"
"Blending_modes_Divisive_Modulo_Continuous_Gradient_Comparison.png"
msgstr ""
".. image:: images/blending_modes/modulo/"
"Blending_modes_Divisive_Modulo_Continuous_Gradient_Comparison.png"

#: ../../reference_manual/blending_modes/modulo.rst:45
msgid ""
"Left: **Base Layer**. Middle: **Blend Layer**. Right: **Divisive Modulo - "
"Continuous**."
msgstr ""
"Ліворуч: **Базовий шар**. Посередині: **Шар змішування**. Праворуч: "
"**Залишок від ділення - неперервний**."

#: ../../reference_manual/blending_modes/modulo.rst:53
msgid ""
"Performs a modulo calculation using the sum of blend layer and the minimum "
"possible value after zero."
msgstr ""
"Виконує обчислення залишку на основі суми значення шару змішування та "
"мінімального можливого значення після нуля."

#: ../../reference_manual/blending_modes/modulo.rst:58
msgid ""
".. image:: images/blending_modes/modulo/"
"Blending_modes_Modulo_Gradient_Comparison.png"
msgstr ""
".. image:: images/blending_modes/modulo/"
"Blending_modes_Modulo_Gradient_Comparison.png"

#: ../../reference_manual/blending_modes/modulo.rst:58
msgid "Left: **Base Layer**. Middle: **Blend Layer**. Right: **Modulo**."
msgstr ""
"Ліворуч: **Базовий шар**. Посередині: **Шар змішування**. Праворуч: "
"**Залишок**."

#: ../../reference_manual/blending_modes/modulo.rst:60
#: ../../reference_manual/blending_modes/modulo.rst:64
msgid "Modulo - Continuous"
msgstr "Залишок — неперервний"

#: ../../reference_manual/blending_modes/modulo.rst:66
msgid ""
"Performs a modulo calculation using the sum of blend layer and the minimum "
"possible value after zero. As this is a continuous mode, anything between "
"odd to even numbers are inverted."
msgstr ""
"Виконує обчислення залишку на основі суми значення шару змішування та "
"мінімального можливого значення після нуля. Оскільки це неперервний режим, "
"усі значення між непарними і парними числами інвертуватимуться."

#: ../../reference_manual/blending_modes/modulo.rst:71
msgid ""
".. image:: images/blending_modes/modulo/"
"Blending_modes_Modulo_Continuous_Gradient_Comparison.png"
msgstr ""
".. image:: images/blending_modes/modulo/"
"Blending_modes_Modulo_Continuous_Gradient_Comparison.png"

#: ../../reference_manual/blending_modes/modulo.rst:71
msgid ""
"Left: **Base Layer**. Middle: **Blend Layer**. Right: **Modulo - "
"Continuous**."
msgstr ""
"Ліворуч: **Базовий шар**. Посередині: **Шар змішування**. Праворуч: "
"**Залишок - неперервний**."

#: ../../reference_manual/blending_modes/modulo.rst:73
#: ../../reference_manual/blending_modes/modulo.rst:77
msgid "Modulo Shift"
msgstr "Зсув за модулем"

#: ../../reference_manual/blending_modes/modulo.rst:79
msgid ""
"Performs a modulo calculation with the result of the sum of base and blend "
"layer by the sum of blend layer with the minimum possible value after zero."
msgstr ""
"Виконує обчислення залишку від ділення суми значень у шарі основи і шарі "
"змішування на суму значення у шарі основи та мінімального можливого значення "
"після нуля."

#: ../../reference_manual/blending_modes/modulo.rst:85
msgid ""
".. image:: images/blending_modes/modulo/"
"Blending_modes_Modulo_Shift_Gradient_Comparison.png"
msgstr ""
".. image:: images/blending_modes/modulo/"
"Blending_modes_Modulo_Shift_Gradient_Comparison.png"

#: ../../reference_manual/blending_modes/modulo.rst:85
msgid "Left: **Base Layer**. Middle: **Blend Layer**. Right: **Modulo Shift**."
msgstr ""
"Ліворуч: **Базовий шар**. Посередині: **Шар змішування**. Праворуч: **Зсув "
"за модулем**."

#: ../../reference_manual/blending_modes/modulo.rst:87
#: ../../reference_manual/blending_modes/modulo.rst:91
msgid "Modulo Shift - Continuous"
msgstr "Зсув за модулем — неперервний"

#: ../../reference_manual/blending_modes/modulo.rst:93
msgid ""
"Performs a modulo calculation with the result of the sum of base and blend "
"layer by the sum of blend layer with the minimum possible value after zero.  "
"As this is a continuous mode, anything between odd to even numbers are "
"inverted."
msgstr ""
"Виконує обчислення залишку від ділення суми значень у шарі основи і шарі "
"змішування на суму значення у шарі основи та мінімального можливого значення "
"після нуля. Оскільки це неперервний режим, усі значення між непарними і "
"парними числами інвертуватимуться."

#: ../../reference_manual/blending_modes/modulo.rst:98
msgid ""
".. image:: images/blending_modes/modulo/"
"Blending_modes_Modulo_Shift_Continuous_Gradient_Comparison.png"
msgstr ""
".. image:: images/blending_modes/modulo/"
"Blending_modes_Modulo_Shift_Continuous_Gradient_Comparison.png"

#: ../../reference_manual/blending_modes/modulo.rst:98
msgid ""
"Left: **Base Layer**. Middle: **Blend Layer**. Right: **Modulo Shift - "
"Continuous**."
msgstr ""
"Ліворуч: **Базовий шар**. Посередині: **Шар змішування**. Праворуч: **Зсув "
"за модулем - неперервний**."
