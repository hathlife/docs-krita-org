msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-06-15 03:16+0200\n"
"PO-Revision-Date: 2019-03-02 16:12-0800\n"
"Last-Translator: Japanese KDE translation team <kde-jp@kde.org>\n"
"Language-Team: Japanese <kde-jp@kde.org>\n"
"Language: ja\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"

#: ../../<rst_epilog>:1
msgid ".. image:: images/Stroke_Selection_4.png"
msgstr ""

#: ../../reference_manual/stroke_selection.rst:1
msgid "How to use the stroke selection command in Krita."
msgstr ""

#: ../../reference_manual/stroke_selection.rst:10
msgid "Selection"
msgstr ""

#: ../../reference_manual/stroke_selection.rst:10
msgid "Stroke"
msgstr ""

#: ../../reference_manual/stroke_selection.rst:15
msgid "Stroke Selection"
msgstr ""

#: ../../reference_manual/stroke_selection.rst:17
msgid ""
"Sometimes, you want to add an even border around a selection. Stroke "
"Selection allows you to do this. It's under :menuselection:`Edit --> Stroke "
"Selection`."
msgstr ""

#: ../../reference_manual/stroke_selection.rst:19
msgid "First make a selection and call up the menu:"
msgstr ""

#: ../../reference_manual/stroke_selection.rst:22
msgid ".. image:: images/Krita_stroke_selection_1.png"
msgstr ""

#: ../../reference_manual/stroke_selection.rst:23
msgid ""
"The main options are about using the current brush, or lining the selection "
"with an even line. You can use the current foreground color, the background "
"color or a custom color."
msgstr ""

#: ../../reference_manual/stroke_selection.rst:25
msgid "Using the current brush allows you to use textured brushes:"
msgstr ""

#: ../../reference_manual/stroke_selection.rst:28
msgid ".. image:: images/Stroke_selection_2.png"
msgstr ""

#: ../../reference_manual/stroke_selection.rst:29
msgid ""
"Lining the selection also allows you to set the background color, on top of "
"the line width in pixels or inches:"
msgstr ""

#: ../../reference_manual/stroke_selection.rst:32
msgid ".. image:: images/Krita_stroke_selection_3.png"
msgstr ""

#: ../../reference_manual/stroke_selection.rst:33
msgid "This creates nice silhouettes:"
msgstr ""
